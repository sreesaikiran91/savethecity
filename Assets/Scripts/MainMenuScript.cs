﻿using UnityEngine;
using System.Collections;

public class MainMenuScript : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void playGame ()
	{
	
		Application.LoadLevel ("Play Game");
	
	}

	public void gameControls ()
	{
		Application.LoadLevel ("Controls");
	}

	public void gameExit ()
	{
	
		Application.Quit ();
	}

	public void gameInstrsuctions ()
	{
	
		Application.LoadLevel ("Instructions");
	}

	public void backMenu ()
	{
	
		Application.LoadLevel ("Game Intro");
	}


}
