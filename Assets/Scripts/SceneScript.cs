using UnityEngine;
using System.Collections;

public class SceneScript : MonoBehaviour
{

	GameObject buildingobj;
	public Transform screenLimiter;
	public int buildingCount;

	// Use this for initialization
	void Start ()
	{
		float posx = -62.9f;
		float posy = -20.45f;
		string [] buildingNames = new string[8]{"Buildings01","Buildings02","Buildings03","Buildings04","Buildings05",
			"Buildings06","Buildings07","Buildings08"};
		
		for (int i=0; i<buildingCount*6; i++) {
			Instantiate (Resources.Load ("Grass Tile"), new Vector2 (posx, posy), Quaternion.identity);
			posx += 7.9f;
		}

		float buidingposx = -6.66f;
		float buildingposy = -14.80f;
		
		for (int j =0; j<buildingCount; j++) {
			buildingobj = (GameObject)Instantiate (Resources.Load (buildingNames [Random.Range (1, 8)]), new Vector2 (buidingposx, buildingposy), Quaternion.identity);
			float buildingscale = buildingobj.GetComponent<Transform> ().localScale.y;

			buildingobj.GetComponent<Transform> ().localScale = new Vector2 (buildingobj.GetComponent<Transform> ().localScale.x, Random.Range (buildingscale, buildingscale + 3.0f));
			buidingposx += 40;
			
		}
		buidingposx += 40;
		buildingposy += 10;
		Instantiate (Resources.Load ("Destinationbuilding"), new Vector2 (buidingposx, buildingposy), Quaternion.identity);

		
		float limiterPosx = screenLimiter.position.x + 2;
		for (int i =0; i<5*buildingCount; i++) {
			Instantiate (screenLimiter, new Vector2 (limiterPosx, screenLimiter.position.y), Quaternion.identity);
			limiterPosx += 4;
		}
		float laserposx = -6.8f;
		float laserposy = 2.3f;
		for (int k =0; k<buildingCount; k++) {
			Instantiate (Resources.Load ("LaserGun"), new Vector2 (laserposx, laserposy), Quaternion.Euler (0, 0, -180));
			laserposy = Random.Range (-1, 2.1f); 
			
			laserposx += 40;
		}

		string [] alienNames = new string[4]{"Alien1","Alien2","Alien3","Alien4"};
		float alienposx = 5.5f;
		//float alienposy = -7;

		for (int i =0; i<buildingCount*3; i++) {
			Instantiate (Resources.Load (alienNames [Random.Range (1, 4)]), new Vector2 (alienposx, Random.Range (-10, -1)), Quaternion.identity);
			alienposx += Random.Range (5, 20);
		}


		float healthpowerposx = 5.5f;
		for (int i =0; i<(buildingCount/2)-10; i++) {
			Instantiate (Resources.Load ("HealthPowerup"), new Vector2 (healthpowerposx, Random.Range (-10, -1)), Quaternion.identity);
			healthpowerposx += Random.Range (5, 20) + 80;
		}

		float laserpowerposx = 5.5f;
		for (int i =0; i<buildingCount/4; i++) {
			Instantiate (Resources.Load ("TorpedoPowerup"), new Vector2 (laserpowerposx, Random.Range (-10, -1)), Quaternion.identity);
			laserpowerposx += Random.Range (5, 20) + 240;
		}



	}
	// Update is called once per frame
	void Update ()
	{
	
	}
}
