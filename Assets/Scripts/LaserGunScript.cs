﻿using UnityEngine;
using System.Collections;

public class LaserGunScript : MonoBehaviour
{


	public AnimationClip anim;
	int lifepoints = 6;
	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
	

	}

	void OnTriggerEnter2D (Collider2D collision)
	{
		
		if (collision.gameObject.tag == "TorpedoNormal") {
			lifepoints--;

			Destroy (collision.gameObject);

			if (lifepoints <= 0) {
				Destroy (this.gameObject);
				GameObject explosion = (GameObject)Instantiate (Resources.Load ("explosion"), new Vector2 (GetComponent<Transform> ().position.x, GetComponent<Transform> ().position.y), Quaternion.identity);

				Destroy (explosion, anim.length);

			}
		}


		if (collision.gameObject.tag == "specialTorpedo") {
			lifepoints -= 2;
			
			Destroy (collision.gameObject);
			
			if (lifepoints <= 0) {
				Destroy (this.gameObject);
				GameObject explosion = (GameObject)Instantiate (Resources.Load ("explosion"), new Vector2 (GetComponent<Transform> ().position.x, GetComponent<Transform> ().position.y), Quaternion.identity);
				
				Destroy (explosion, anim.length);
				
			}
		}


		if (collision.gameObject.tag == "Player") {
		
			GameObject obj;
			obj = GameObject.FindGameObjectWithTag ("Player");
			obj.GetComponent<PlayerScript> ().playerHealth -= 20; 


		}
	}
}
