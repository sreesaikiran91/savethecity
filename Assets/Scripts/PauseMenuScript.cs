using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PauseMenuScript : MonoBehaviour
{
	bool isPaused = false;

	public Button playbtn;
	public GameObject pauseMenu;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{

		if (isPaused) {
		
			pauseMenu.SetActive (true);

			Time.timeScale = 0;

		} else {
		
			pauseMenu.SetActive (false);
			Time.timeScale = 1;
		}

	
	}


	public void gamePause ()
	{

		isPaused = !isPaused;
		
		
	}

	public void gameResume ()
	{
	
		isPaused = !isPaused;
	
	}


}
