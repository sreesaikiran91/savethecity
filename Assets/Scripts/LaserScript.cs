using UnityEngine;
using System.Collections;

public class LaserScript : MonoBehaviour
{

	public AnimationClip anim;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnCollisionEnter2D (Collision2D collision)
	{
	
		if (collision.gameObject.tag == "TorpedoNormal") {
			Destroy (collision.gameObject);
		
		}
		if (collision.gameObject.tag == "specialTorpedo") {
			Destroy (collision.gameObject);
			
		}
		if (collision.gameObject.tag == "alienTorpedo") {
			Destroy (collision.gameObject);
			
		}
		if (collision.gameObject.tag == "Player") {
			GameObject obj;
			obj = GameObject.FindGameObjectWithTag ("Player");
			GameObject explosion = (GameObject)Instantiate (Resources.Load ("playerexplosion"), new Vector2 (obj.GetComponent<Transform> ().position.x, obj.GetComponent<Transform> ().position.y), Quaternion.identity);


			obj.GetComponent<PlayerScript> ().playerspawn ();
			Destroy (explosion, anim.length);
		}

	}
}
