using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{


	public Transform torpedofire;
	public float MAX_SPEED = 12.0f;
	public float Vertical_Max_Speed;
	float yAxis;
	public AnimationClip anim;
	public Text healthtxt;
	public Text blastedTXT;
	GameObject smokeobj;
	public Text Gameoverscore;
	public GameObject gameoverCanvas;
	AudioSource aud;
	public GameObject gamecompleteCanvas;
	public AudioClip powerUpSound;
	public AudioClip playerBlastSound;
	public AudioClip collisionSound;
	public AudioClip GameOverSound;
	public Text GameWonScore;
	public AudioClip fireSound;
	public AudioSource backgroundmusic;
	public Text LifesTXT;
	public int playerlifes = 3;
	public Text scoreTXT;
	public int playerScore = 0;
	public AnimationClip collisionBlast;
	//float speedMultiplier = 1f;
	bool isspecialTorpedo = false;
	GameObject smoke;
	public int playerHealth = 100;

	// Use this for initialization
	void Start ()
	{
		smokeobj = GameObject.FindGameObjectWithTag ("smoke");

		blastedTXT.text = "";

	}



	// Update is called once per frame
	void Update ()
	{

		GameWonScore.text = playerScore.ToString ();
		Gameoverscore.text = playerScore.ToString ();


		if (playerHealth < 50) {

			smokeobj.GetComponent<SpriteRenderer> ().enabled = true;

		} else {

			smokeobj.GetComponent<SpriteRenderer> ().enabled = false;
		}



		if (playerHealth <= 0) {
			playerspawn ();
		}
		scoreTXT.text = playerScore.ToString ();
		healthtxt.text = playerHealth.ToString ();
		LifesTXT.text = playerlifes.ToString ();

		if (Input.GetKeyDown ("space")) {


			if (isspecialTorpedo) {
				Instantiate (Resources.Load ("SpecialTorpedo"), new Vector2 (torpedofire.position.x, torpedofire.position.y), Quaternion.Euler (0, 0, 0));
				aud = GetComponent<AudioSource> ();
				aud.PlayOneShot (fireSound, 0.9F);

			} else {

				Instantiate (Resources.Load ("laserNormal"), new Vector2 (torpedofire.position.x, torpedofire.position.y), Quaternion.Euler (0, 0, -90));
				aud = GetComponent<AudioSource> ();
				aud.PlayOneShot (fireSound, 0.9F);

			}

		}



		GetComponent<Rigidbody2D> ().velocity = new Vector2 (MAX_SPEED, MAX_SPEED * Input.GetAxis ("Vertical"));

	}

	void OnTriggerEnter2D (Collider2D collision)
	{

		if (collision.gameObject.tag == "alien") {
			GameObject colblast = (GameObject)Instantiate (Resources.Load ("CollisionBlast"), new Vector2 (this.GetComponent<Transform> ().position.x + 1.5f, this.GetComponent<Transform> ().position.y), Quaternion.identity);
			Destroy (colblast, collisionBlast.length);
			aud = GetComponent<AudioSource> ();
			aud.PlayOneShot (collisionSound, 0.9F);

			playerHealth -= 20;
			healthtxt.text = playerHealth.ToString ();
			if (playerHealth <= 0) {
				GameObject explosion = (GameObject)Instantiate (Resources.Load ("playerexplosion"), new Vector2 (this.GetComponent<Transform> ().position.x, this.GetComponent<Transform> ().position.y), Quaternion.identity);
				Destroy (explosion, anim.length);
				playerspawn ();
			}



		}

		if (collision.gameObject.tag == "torpedoPowerup") {

			isspecialTorpedo = true;
			Destroy (collision.gameObject);
			aud = GetComponent<AudioSource> ();
			aud.PlayOneShot (powerUpSound, 0.9F);

		}

		if (collision.gameObject.tag == "destination") {

			gamecompleteCanvas.SetActive (true);
			Destroy (this.gameObject);
		}



		if (collision.gameObject.tag == "healthPowerup") {


			Destroy (collision.gameObject);
			if (playerHealth <= 50 && playerHealth > 0) {
				playerHealth += 50;


			} else {

				playerHealth += 100 - playerHealth;


			}
			aud = GetComponent<AudioSource> ();
			aud.PlayOneShot (powerUpSound, 0.9F);
		}




	}

	public void playerspawn ()
	{

		if (playerlifes <= 0) {
			LifesTXT.text = playerlifes.ToString ();
			aud = GetComponent<AudioSource> ();
			aud.PlayOneShot (GameOverSound, 1);
			Destroy (this.gameObject);
			Time.timeScale = 0;
			gameoverCanvas.SetActive (true);


		} else {


			playerlifes--;
			aud = GetComponent<AudioSource> ();
			aud.PlayOneShot (playerBlastSound, 1.0f);
			LifesTXT.text = playerlifes.ToString ();
			smokeobj.GetComponent<SpriteRenderer> ().enabled = false;

			this.GetComponent<Transform> ().position = new Vector2 (this.GetComponent<Transform> ().position.x - 30, -8.4f);
			StartCoroutine ("playerCrash");
			playerHealth = 100;
			isspecialTorpedo = false;
		}



	}

	IEnumerator playerCrash ()
	{

		blastedTXT.text = "You Crashed !!!";
		Time.timeScale = 0.1f;
		float pauseEndTime = Time.realtimeSinceStartup + 2;
		while (Time.realtimeSinceStartup < pauseEndTime) {
			yield return 0;
		}
		Time.timeScale = 1;
		blastedTXT.text = "";


	}



}






