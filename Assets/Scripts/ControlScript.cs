﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlScript : MonoBehaviour
{

	bool paused;
	//public Button pausebtn;
	//public Button playbtn;
	// Use this for initialization
	void Start ()
	{
		paused = false;


	}
	
	// Update is called once per frame
	void Update ()
	{
	

	}

	public void Pausegame ()
	{
		paused = !paused;
		
		if (paused && !Input.GetKeyDown ("space")) {
			Time.timeScale = 0;

		} else if (!paused) {
			Time.timeScale = 1;


		}
	}
}
