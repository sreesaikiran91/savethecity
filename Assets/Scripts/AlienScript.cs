using UnityEngine;
using System.Collections;

public class AlienScript : MonoBehaviour
{

	int lifepoints = 2;
	public Transform shooter;
	public AnimationClip anim;
	float originalY;
	GameObject playerobj;
	float floatStrength = 3;
	GameObject alienTorpedo;
	// Use this for initialization
	void Start ()
	{
		this.originalY = this.transform.position.y;

	}
	
	// Update is called once per frame
	void Update ()
	{

		//transform.Translate (0*Time.deltaTime,5* Time.deltaTime,0.0f);

		transform.position = new Vector3 (transform.position.x,
		                                 originalY + ((float)System.Math.Sin (Time.time) * floatStrength),
		                                 transform.position.z);
		playerobj = GameObject.FindGameObjectWithTag ("Player");




	
	}

	void OnTriggerEnter2D (Collider2D collision)
	{
		
		if (collision.gameObject.tag == "TorpedoNormal") {

			lifepoints--;
			Destroy (collision.gameObject);
			if (lifepoints <= 0) {
				playerobj.GetComponent<PlayerScript> ().playerScore += 10; 
				Destroy (this.gameObject);
				GameObject explosion = (GameObject)Instantiate (Resources.Load ("alienexplosion"), new Vector2 (GetComponent<Transform> ().position.x, GetComponent<Transform> ().position.y), Quaternion.identity);
				Destroy (explosion, anim.length);


			}
		}
		if (collision.gameObject.tag == "specialTorpedo") {

			lifepoints -= 2;
			Destroy (collision.gameObject);
			if (lifepoints <= 0) {
				playerobj.GetComponent<PlayerScript> ().playerScore += 10; 
				Destroy (this.gameObject);
				GameObject explosion = (GameObject)Instantiate (Resources.Load ("alienexplosion"), new Vector2 (GetComponent<Transform> ().position.x, GetComponent<Transform> ().position.y), Quaternion.identity);
				Destroy (explosion, anim.length);
				
				
			}
		}


	}
}
